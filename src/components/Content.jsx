import React from 'react'
import ImageOne from '../images/egg.jpg'
import ImageTwo from '../images/egg2.jpg'

const Content = () => {
    return (
        <>
            <div className="menu-card">
                <img src={ImageOne} alt="egg" className="h-full rounded mb-20 shadow" />
                <div className="content-center">
                    <h2 className="text-2xl mb-2 text-primary">Egg Burger</h2>
                    <p className="mb-2 text-secondary">Delicious and nutritious</p>
                    <span className="text-primary">$16</span>
                </div>
            </div>
            <div className="menu-card">
                <img src={ImageTwo} alt="egg" className="h-full rounded mb-20 shadow" />
                <div className="content-center">
                    <h2 className="text-2xl mb-2 text-primary">Scrambled Eggs</h2>
                    <p className="mb-2 text-secondary">Delicious and nutritious</p>
                    <span className="text-primary">$14</span>
                </div>
            </div>
        </>
    )
}

export default Content
