import React from 'react';
import { Link } from 'react-router-dom'
import useDarkMode from '../hooks/useDarkMode';

const Navbar = ({ toggle }) => {
    const [colorTheme, setTheme] = useDarkMode()
    return (
        <nav className="flex justify-between items-center h-16 bg-secondary text-primary
        relative shadow-sm font-mono" role="navigation">
            <div className="flex justify-center items-center">
                <span onClick={() => setTheme(colorTheme)} className="w-10 h-10 ml-2 bg-accent rounded-full shadow-lg cursor-pointer 
            text-white flex items-center justify-center">
                    {colorTheme === 'light' ?
                        <svg className="w-6 h-6"
                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9.663 17h4.673M12 3v1m6.364 1.636l-.707.707M21 
                        12h-1M4 12H3m3.343-5.657l-.707-.707m2.828 9.9a5 5 0 117.072 0l-.548.547A3.374 3.374 0 0014 18.469V19a2 2 0 
                        11-4 0v-.531c0-.895-.356-1.754-.988-2.386l-.548-.547z" />
                        </svg>
                        :
                        <svg className="w-6 h-6"
                            xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 
                        0 0012 21a9.003 9.003 0 008.354-5.646z" />
                        </svg>
                    }
                </span>
                <Link to='/' className="pl-5">
                    CHEAT MEAL
                </Link>
            </div>
            <div className="px-4 cursor-pointer md:hidden" onClick={toggle}>
                <svg className="w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16M4 18h16" />
                </svg>
            </div>
            <div className="pr-8 md:block hidden">
                <Link className="p-4" to="/">Accueil</Link>
                <Link className="p-4" to="/menu">Menu</Link>
                <Link className="p-4" to="/about">A Propos</Link>
                <Link className="p-4" to="/contact">Contact</Link>
            </div>
        </nav>
    )
}

export default Navbar
